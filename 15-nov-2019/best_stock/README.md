# BestStock

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `best_stock` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:best_stock, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/best_stock](https://hexdocs.pm/best_stock).

