defmodule BestStock do
  @moduledoc """
  Documentation for BestStock.
  """

  @doc """
  Hello world.

  ## Examples

      iex> BestStock.hello()
      :world

  """
  def get_last_price(stock_ticker) do
    stock_details = StockPriceFetcher.fetch(stock_ticker)

    [stock_details["Name"], stock_details["LastPrice"]]
  end

  def find(stock_tickers) do
    stock_tickers
    |> Enum.map(&get_last_price(&1))
    |> Enum.sort(&(Enum.at(&1,1) >= Enum.at(&2,1)))
    |> Enum.at(0)
  end
end
