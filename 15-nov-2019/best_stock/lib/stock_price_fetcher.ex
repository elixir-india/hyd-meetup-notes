defmodule StockPriceFetcher do
  @moduledoc """
  Documentation for StockPriceFetcher.
  """

  @doc """
  Hello world.

  ## Examples

      iex> StockPriceFetcher.fetch("FB")

  """
  def fetch(stock_code) do
    api_url = "http://dev.markitondemand.com/MODApis/Api/v2/Quote/json?symbol=#{stock_code}" 

    %HTTPoison.Response{status_code: 200, body: body} = HTTPoison.get! api_url
    Jason.decode! body
  end
end
