# atoms
# pattern matching

# gaurd functions

# eco system(mix, ecto, phoeniix, liveview, release, umbrella, ETS)
# Bleachers report, Pintrest
# defmodule Employee do

#   age = 25
#   def inc_age(age) do
#     age + 1
#   end

#   IO.puts age
# end


# handle_open = fn do
#   {:ok, file} -> "Read data: #{IO.read(file, :line)}"
#   {:error, message} -> "Error message #{message}"
#   end

# defmodule Chain do
#   def counter(next_pid) do
#     receive do
#     n ->
#     # IO.puts "PID: #{inspect next_pid}, val: #{n}"
#     send next_pid, n + 1
#     end
#   end

#   def create_processes(n) do
#     last = Enum.reduce 1..n, self,
#                                   fn (_,send_to) ->
#                                   spawn(Chain, :counter, [send_to])
#                                   end

#   # start the count by sending
#     send last, 0

#   # and wait for the result to come back to us
#     receive do
#       final_answer when is_integer(final_answer) ->
#       "Result is #{inspect(final_answer)}"
#       end
#   end

#   def run(n) do
#   IO.puts inspect :timer.tc(Chain, :create_processes, [n])
#   end
# end
# ------------------
defmodule Roman do
  @doc """
  Convert the number to a roman number.
  """
  @romanConvert%{
    1    => "I",
    5    => "V",
    10   => "X",
    50   => "L",
    100  => "C",
    500  => "D",
    1000 => "M"
  }

  @spec numerals(pos_integer) :: String.t()
  def numerals(number) do
    numeral(number,"")
  end

  def numeral(number,collect) do
    cond  do
      number>=1000 ->
        numeral(number-(div(number,1000)*1000), collect <> num(div(number,1000)*1000))
      number>=100 && number<1000 ->
        numeral(number-(div(number,100)*100), collect <> num(div(number,100)*100))
      number<100 && number>=10 ->
        numeral(number-(div(number,10)*10), collect <> num(div(number,10)*10))
      number<10 && number>=1 ->
        collect <> num(number)
      number==0 ->
        collect
    end
  end

  def num(n) when n>=1000 do
    tempNum=div(n,1000)
    String.duplicate(@romanConvert[1000],tempNum)
  end
  def num(n) when n<1000 and n>=500 do
    tempNum=div(n,100)
    if tempNum==9 do
     @romanConvert[100] <> @romanConvert[1000]
    else
     @romanConvert[500] <> String.duplicate(@romanConvert[100],tempNum-5)
    end
  end
  def num(n) when n<500 and n>=100 do
    tempNum=div(n,100)
    if tempNum==4 do
     @romanConvert[100] <> @romanConvert[500]
    else
     String.duplicate(@romanConvert[100],tempNum)
    end
  end
  def num(n) when n<100 and n>=50 do
    tempNum=div(n,10)
    if tempNum==9  do
     @romanConvert[10] <> @romanConvert[100]
    else
     @romanConvert[50] <> String.duplicate(@romanConvert[10],tempNum-5)
    end
  end
  def num(n) when n<50 and n>=10 do
    tempNum=div(n,10)
    if tempNum==4  do
     @romanConvert[10] <> @romanConvert[50]
    else
     String.duplicate(@romanConvert[10],tempNum)
    end
  end
  def num(n) when n<10 and n>=5 do
    if n==9 do
     @romanConvert[1] <> @romanConvert[10]
    else
     @romanConvert[5] <> String.duplicate(@romanConvert[1],n-5)
    end
  end
  def num(n) when n<5 and n>=1 do
    if n==4 do
     @romanConvert[1] <> @romanConvert[5]
    else
     String.duplicate(@romanConvert[1],n)
    end
  end
  def num(n) when n==0 do
    0
  end
end

# ---------------
# class Employee:
#     def __init__(self, name, age, salary):
#         self.name = name
#         self.age = age
#         self.salary = salary
#     def inc(self):
#         self.age = self.age+1
#         print(self.age)

# abhay = Employee('abhay',25,1000)
# abhay.inc()
# abhay.inc()
